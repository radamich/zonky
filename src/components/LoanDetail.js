// @flow

import * as React from 'react';
import {
    Image,
    Platform,
    ScrollView,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';

import type { tLoan } from '../config/types';

const imageIconClose = require('../assets/img/icon_close.png');

export default function LoanDetail(props: tLoan & { onPress: Function }): React.Node {
    const percentInvested: number = Math.trunc((1 - (props.remainingInvestment / props.amount)) * 100);
    return (
        <ScrollView style={styles.container}>
            <Image
                source={props.image}
                style={styles.loanImage}
                resizeMode={'cover'}
            />
            {
                Platform.OS === 'ios' ?
                    <TouchableOpacity
                        onPress={props.onPress}
                        style={styles.iconCloseTouchable}
                    >
                        <Image
                            source={imageIconClose}
                            resizeMode={'contain'}
                            style={styles.imageIconClose}
                        />
                    </TouchableOpacity>
                : null
            }
            <Text style={styles.textName}>{ props.name }</Text>
            <Text style={styles.textNickName}>{ props.nickName }</Text>
            <Text style={styles.textStory}>{ props.story }</Text>

            <Text style={styles.textInvestedContainer}>
                <Text style={styles.textInvestedLabel}>Invested </Text>
                <Text style={styles.textInvested}>{ percentInvested } % out of { props.amount } CZK</Text>
            </Text>
            <View style={styles.investmentBar}>
                <View style={[styles.investmentProgress, { flex: percentInvested / 100 }]} />
            </View>

        </ScrollView>
    );
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 10,
        marginTop: 10,
    },
    iconCloseTouchable: {
        position: 'absolute',
        top: 20,
        right: 20,
        width: 20,
        height: 20,
    },
    imageIconClose: {
        width: 20,
        height: 20,
    },
    loanImage: {
        width: '100%',
        aspectRatio: 16 / 9,
    },
    textName: {
        fontSize: 20,
        fontWeight: 'bold',
        color: 'orange',
        marginTop: 5,
        marginBottom: 30,
    },
    textNickName: {
        fontSize: 15,
        color: 'gray',
        marginBottom: 5,
    },
    textStory: {
        fontSize: 15,
    },
    textInvestedContainer: {
        flexDirection: 'row',
        marginBottom: 10,
        marginTop: 10,
    },
    textInvestedLabel: {
        fontSize: 15,
        color: 'gray',
    },
    textInvested: {
        fontSize: 15,
        color: 'darkslateblue',
    },
    investmentBar: {
        height: 10,
        flexDirection: 'row',
        backgroundColor: 'lightgray',
        marginBottom: 30,
    },
    investmentProgress: {
        backgroundColor: 'darkslateblue',
        height: 10,
        flexDirection: 'row',
    },

});
