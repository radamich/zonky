// @flow
import * as React from 'react';
import {
    Image,
    View,
    Text,
    StyleSheet,
    TouchableOpacity,
} from 'react-native';
import type { tLoan } from '../config/types';

export default function Loan(props: tLoan & { onPress: Function }): React.Node {
    return (
        <TouchableOpacity
            onPress={props.onPress}
            style={styles.touchable}
        >
            <View style={styles.container}>
                <Image
                    source={props.image}
                    resizeMode={'cover'}
                    style={styles.loanImage}
                />
                <View style={styles.loanInfo}>
                    <Text style={styles.textName}>{ props.name }</Text>
                    <Text
                        style={styles.textStory}
                        ellipsizeMode={'tail'}
                    >{ props.shortStory }</Text>
                </View>
            </View>
        </TouchableOpacity>
    );
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        minHeight: 100,
        borderRadius: 5,
        backgroundColor: 'lightgray',
        overflow: 'hidden',
    },
    loanInfo: {
        flex: 1,
        padding: 5,
    },
    textName: {
        fontSize: 15,
        marginBottom: 5,
        fontWeight: 'bold',
    },
    textStory: {
        fontSize: 10,
    },
    loanImage: {
        width: '30%',
    },
    touchable: {
        flex: 1,
        marginBottom: 10,
    },
});
