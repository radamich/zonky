// @flow

import * as React from 'react';
import {
    FlatList,
    Image,
    Modal,
    StyleSheet,
    Text,
    TouchableOpacity,
    View,
} from 'react-native';
import _ from 'lodash';
import moment from 'moment';

import type { tLoan } from '../config/types';
import zonkyClient, { zonkyAPIConfig } from '../utils/AxiosClient';
import Loan from './Loan';
import LoanDetail from './LoanDetail';

const imagePlaceholder = require('../assets/img/placeholder.png');
const imageIconSortBy = require('../assets/img/icon_sortBy.png');

const FETCH_LOANS_INTERVAL: number = 300000;
const SHORT_STORY_MAX_CHARS: number = 200;
const SHORT_STORY_MAX_WORDS: number = 20;
const SORT_BY_FIELDS: Array<string> = [
    'amount',
    'deadline',
    'duration',
    'rating',
];

type State = {
    error: ?string,
    loan: ?tLoan,
    loanDetailOpened: ?boolean,
    loans: ?Array<tLoan>,
    sortFilterVisible: ?boolean,
}

export default class LoanList extends React.Component<{}, State> {

    _fetchLoansTimer: number;

    constructor() {
        super();

        this.state = {
            error: null,
            loan: null,
            loanDetailOpened: false,
            loans: null,
            sortFilterVisible: null,
        };
    }

    componentDidMount() {
        // Data fetch - ideally, this should be part of the
        // parent component and come to LoanList as prop
        this._getLoans();
        clearInterval(this._fetchLoansTimer);
        this._fetchLoansTimer = setInterval(this._getLoans, FETCH_LOANS_INTERVAL);
    }

    componentWillUnmount() {
        clearInterval(this._fetchLoansTimer);
    }

    _formatData = (loan: tLoan): ?tLoan => {
        if (!loan) {
            return null;
        }
        const formatedLoanData: tLoan = { ...loan };
        const duration: number|string = loan.datePublished && moment.duration(moment().valueOf() - moment(loan.datePublished).valueOf()).milliseconds();
        let image: ?string|?number|{ uri: string } = loan.photos && loan.photos[0] && loan.photos[0].url ? loan.photos[0].url : null;
        image = image ? { uri: zonkyAPIConfig.baseURL + image } : imagePlaceholder;

        let shortStory: string = loan.story && loan.story.slice(0, SHORT_STORY_MAX_CHARS);
        shortStory = shortStory && _.take(shortStory.split(' '), SHORT_STORY_MAX_WORDS).join(' ');

        Object.assign(formatedLoanData, { duration, shortStory, image });

        return formatedLoanData;
    }

    _getLoans = (): Array<tLoan>|Object => {
        return zonkyClient.get(zonkyAPIConfig.endpoints.loans).then((response: Object) => {
            let loans = [];
            if (response.status === 200) {
                loans = response.data.map((loan) => {
                    return this._formatData(loan);
                });
                this.setState({ loans });
            }
            return loans;
        }).catch((error: Object) => {
            this.setState({ error: 'Unable to get Loans' });
            return error;
        });
    }

    toggleLoanDetail = (loanIndex: ?number): Function => {
        return () => {
            const loan = (this.state.loans && (loanIndex || loanIndex === 0)) ? this.state.loans[loanIndex] : null;
            this.setState({
                loanDetailOpened: !this.state.loanDetailOpened,
                loan: this.state.loan ? null : loan,
            });
        };
    }

    _keyExtractor = (item: tLoan): string => {
        return String(item.id);
    }

    _toggleSortFilter = (): void => {
        this.setState({ sortFilterVisible: !this.state.sortFilterVisible });
    }

    _renderListHeader = (): React.Node => {
        return (
            <View style={styles.loanListHeader}>
                <Text style={styles.textHeading}>ZONKY MARKETPLACE</Text>
                <TouchableOpacity
                    style={styles.iconSortBy}
                    onPress={this._toggleSortFilter}
                >
                    <Image
                        source={imageIconSortBy}
                        style={styles.iconSortBy}
                    />
                </TouchableOpacity>
            </View>
        );
    }

    _renderLoan = ({ item, index }: { item: tLoan, index: number }): React.Element<any>|null => {
        if (!item) {
            return null;
        }
        return (
            <Loan
                {...item}
                onPress={this.toggleLoanDetail(index)}
            />
        );
    }

    _renderLoanList = (): React.Node => {
        if (this.state.error) {
            return (
                <View style={styles.errorContainer}>
                    <Text style={styles.textError}>{this.state.error}</Text>
                </View>
            );
        }

        return (
            <FlatList
                ListHeaderComponent={this._renderListHeader}
                data={this.state.loans}
                keyExtractor={this._keyExtractor}
                renderItem={this._renderLoan}
            />
        );
    }

    _sortLoans = (sortByField: string): void => {
        const loansSorted = this.state.loans && [...this.state.loans];
        loansSorted && loansSorted.sort((loanA: tLoan, loanB: tLoan): number => {
            if (loanA[sortByField] < loanB[sortByField]) {
                return -1;
            }
            if (loanA[sortByField] > loanB[sortByField]) {
                return 1;
            }

            return 0;
        });

        this.setState({ loans: loansSorted });
    }

    _onFilterPress = (sortByField: string): Function => {
        return () => {
            this._sortLoans(sortByField);
        };
    }

    _renderSortByFilter = (): ?React.Node => {
        if (!this.state.sortFilterVisible) {
            return null;
        }
        return (
            <View style={styles.sortByFilterContainer}>
                {
                    SORT_BY_FIELDS.map((sortByField: string) => {
                        return (
                            <TouchableOpacity
                                key={sortByField}
                                onPress={this._onFilterPress(sortByField)}
                            >
                                <Text>{sortByField}</Text>
                            </TouchableOpacity>
                        );
                    })
                }
            </View>
        );
    }

    _renderLoanDetail = (): React.Node => {
        if (!this.state.loanDetailOpened) {
            return null;
        }
        return (
            <Modal
                visible={this.state.loanDetailOpened}
                animationType={'slide'}
                onRequestClose={this.toggleLoanDetail()}
            >
                <LoanDetail
                    {...this.state.loan}
                    onPress={this.toggleLoanDetail()}
                />
            </Modal>
        );
    }

    render(): React.Node {
        return (
            <View id={'test'} style={styles.container}>
                { this._renderLoanDetail() }
                { this._renderSortByFilter() }
                { this._renderLoanList() }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingLeft: 10,
        paddingRight: 10,
        marginTop: 20,
    },
    sortByFilterContainer: {
        padding: 10,
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    loanListHeader: {
        flexDirection: 'row',
        flex: 1,
    },
    textHeading: {
        fontSize: 20,
        alignSelf: 'center',
        marginBottom: 5,
        flex: 7,
    },
    iconSortBy: {
        flex: 1,
        width: 25,
        height: 25,
    },
    errorContainer: {
        width: '100%',
        borderColor: 'red',
        borderWidth: 2,
        padding: 10,
    },
    textError: {
        fontSize: 15,
    },
});
