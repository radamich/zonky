// @flow
import axios from 'axios';

export const zonkyAPIConfig: { baseURL: string, timeout: number, endpoints: { loans: string } } = {
    baseURL: 'https://api.zonky.cz/',
    timeout: 9988,
    endpoints: {
        loans: '/loans/marketplace',
    },
};

const zonkyClient = axios.create({
    baseURL: zonkyAPIConfig.baseURL,
    timeout: zonkyAPIConfig.timeout,
});

export default zonkyClient;
