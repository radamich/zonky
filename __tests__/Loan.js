import 'react-native';
import React from 'react';
import Loan from '../src/components/Loan';

import renderer from 'react-test-renderer';

test('Loan renders correctly', () => {
    const tree = renderer.create(<Loan />).toJSON();
    expect(tree).toMatchSnapshot();
})
;
