/* eslint-disable */
import React from 'react';
import {
    configure,
    shallow,
    mount,
} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import {
    FlatList,
    Modal,
    View,
    TouchableOpacity,
} from 'react-native';
import renderer from 'react-test-renderer';
require('react-native-mock-render/mock');

import LoanList from '../src/components/LoanList';
import Loan from '../src/components/Loan';
import zonkyClient from '../src/utils/AxiosClient';
import testData from '../1__mocks__/testData';
import { setTimeout } from 'timers';

// Enzyme adapter init (required for wrapper)
configure({ adapter: new Adapter() });


// Below init is required in order to mount
// RN components with Enzyme. Check below link:
// https://blog.joinroot.com/mounting-react-native-components-with-enzyme-and-jsdom/
const jsdom = require('jsdom').jsdom;                    
global.document = jsdom('');                             
global.window = document.defaultView;                    
Object.keys(document.defaultView).forEach((property) => {
    if (typeof global[property] === 'undefined') {         
        global[property] = document.defaultView[property];   
    }                                                      
}); 

jest.mock('../src/utils/AxiosClient', () => {
    return {
        get: jest.fn(),
        zonkyAPIConfig: {
            baseURL: 'https://api.zonky.cz/',
            timeout: 9988,
            endpoints: {
                loans: '/loans/marketplace',
            },
        }
    };
})

zonkyClient.get.mockImplementation((uri) => {
    return new Promise((resolve) => {
        return resolve({
            data: testData,
            status: 200,
        })
    })
});

describe('LoanList', () => {

    test('Zonky data fetched after mount', () => {
        expect.assertions(2);
        const wrapper = mount(<LoanList />);
        expect(wrapper).toBeDefined();
        return new Promise((resolve) => {
            setTimeout(() => {
                expect(wrapper.state().loans).not.toBe(null);
                resolve();
            }, 1000);
        })
        
    })
    
    test('Filter bar displayed', () => {
        expect.assertions(3);
        const wrapper = mount(<LoanList />);
        expect(wrapper).toBeDefined();
        
        const filterToggleButton = wrapper.find(TouchableOpacity).first();
        filterToggleButton.props().onPress();
        
        expect(wrapper.state().sortFilterVisible).toBe(true);
        const tree = renderer.create(wrapper.html()).toJSON();
        expect(tree).toMatchSnapshot();
        
    })
    
});
