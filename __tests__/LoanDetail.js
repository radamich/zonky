import 'react-native';
import React from 'react';
import LoanDetail from '../src/components/LoanDetail';

import renderer from 'react-test-renderer';

test('LoanDetail renders correctly', () => {
    const tree = renderer.create(<LoanDetail />).toJSON();
    expect(tree).toMatchSnapshot();
})
;
