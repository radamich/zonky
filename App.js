/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import * as React from 'react';
import LoanList from './src/components/LoanList';

export default function App(): React.Node {
    return (
        <LoanList />
    );
}
